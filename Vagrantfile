# yamlファイルから設定の読み込み
require "yaml"
# =============================================================================
# パラメータは「./vagrant/group_vars/all.yml」で編集
# =============================================================================
params = YAML.load_file("./ansible_local/group_vars/all.yml")

# インストールされていないプラグインをインストール
[
  "vagrant-vbguest",
  "vagrant-proxyconf"
].each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin?(plugin)
end

# Vagrantの設定
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.box_check_update = true

  # VirtualBox固有の設定
  config.vm.provider "virtualbox" do |vb|
    vb.customize [
      "modifyvm", :id,
      "--paravirtprovider", "kvm", # Vagrant で VirtualBox 5.0 の準仮想化を有効にする
    ]
  end

  # host名の設定
  config.vm.hostname = params["hostname"]

  # ipaddressの設定(http://labs.septeni.co.jp/entry/20140707/1404670069)
  config.vm.network :private_network, ip: params["ip_address"]

  # ポートの設定
  config.vm.network :forwarded_port, guest: 80,  host: params["http_port"],  id: "http", auto_correct: true, host_ip: "127.0.0.1"
  config.vm.network :forwarded_port, guest: 22,  host: params["ssh_port"],   id: "ssh",  auto_correct: true, host_ip: "127.0.0.1"
  config.vm.network :forwarded_port, guest: 25,  host: params["smtp_port"],  id: "smtp", auto_correct: true, host_ip: "127.0.0.1"
  config.vm.network :forwarded_port, guest: 110, host: params["pop_port"],   id: "pop",  auto_correct: true, host_ip: "127.0.0.1"

  # Proxyの設定
#  if Vagrant.has_plugin?("vagrant-proxyconf")
#    config.proxy.http     = "http://192.168.0.1:1234/"
#    config.proxy.https    = "http://192.168.0.1:1234/"
#    config.proxy.no_proxy = "localhost,127.0.0.1"
#  end

  # フォルダ同期の設定
  config.vm.synced_folder ".", params["web_server_root"], type: "virtualbox"

  # Ansibleでの初期設定
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook       = "./ansible_local/playbook.yml"
    ansible.inventory_path = "./ansible_local/hosts/development"
    ansible.limit          = "all"
  end

  # shellで初期設定(ユーザ vagrant で shell を実行。provision 時には sudo を使用して shell が実行されている)
  config.vm.provision "shell", run: "always", inline: <<-SHELL
    # yum updateを毎回実行する
    echo "yum update =========================================================="
    yum update -y

    # メールログが出力されないことへの対策(https://qiita.com/t_Signull/items/7d557ae3b272bd1ae78f)
    echo "メールログ対策 ======================================================"
    rm /var/lib/rsyslog/imjournal.state
    systemctl restart rsyslog

    echo "php-fpm 再起動 ======================================================"
    systemctl restart php-fpm

    # storage配下の30日以上前のファイルを削除する
    echo "30日以上前の不要ファイルの削除 ======================================"
    find #{params['web_server_storage']}/logs        -type f -name '*' -not -name '.gitignore' -mtime +30 -delete
    find #{params['web_server_storage']}/server_logs -type f -name '*' -not -name '.gitignore' -mtime +30 -delete
    find #{params['web_server_storage']}/app         -type f -name '*' -not -name '.gitignore' -mtime +30 -delete

    echo https://#{params["ip_address"]}/
    echo https://#{params["ip_address"]}/__clockwork
    echo vagrant ssh
    echo cd #{params["web_server_root"]}
  SHELL
end
